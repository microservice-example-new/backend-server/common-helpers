'use strict';

((grpcConnectorHelper) => {

    const path = require('path');
    const protoLoader = require('@grpc/proto-loader');

    const PROTO_PATH = path.join(__dirname, '../../../', 'application-protos');
    grpcConnectorHelper.connectGrpc = (grpc, filename) => {
        const packageDefinition = protoLoader.loadSync(`${PROTO_PATH}/${filename}`, {
            keepCase: true,
            longs: String,
            defaults: true,
            oneofs: true,
        });

        return grpc.loadPackageDefinition(packageDefinition);

    }
})(module.exports);